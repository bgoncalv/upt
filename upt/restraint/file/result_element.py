"""Restraint Result Element."""

import copy
import typing
import xml.etree.ElementTree as ET

from .common_element import CommonXMLElement
from .simple_elements import RestraintLog
from .utils import generate_empty_element


class RestraintResult(CommonXMLElement):
    """Result class.

    This class includes a list of logs.
    You can see an example in examples.py file (result_example).
    """

    _attributes = ['id', 'path', 'result']
    _name = 'result'
    _delegated_items = ['logs']

    def __init__(self, element: ET.Element):
        """Initialize."""
        super().__init__(element)
        self.logs = self._process_logs()
        self.text = self.element.text.strip() if self.element.text else ''
        self._clear()

    def _process_logs(self) -> typing.List[RestraintLog]:
        """Generate the list of logs."""
        logs: typing.List[RestraintLog] = []
        if (xml_logs := self.element.find('.//logs')) is not None:
            for log in xml_logs.findall('log'):
                logs.append(RestraintLog(log))
        return logs

    def generate_xml_object(self) -> ET.Element:
        """Recreate and send the xml object."""
        self._clear()
        if self.logs:
            logs = generate_empty_element('logs')
            for log in self.logs:
                logs.append(log.generate_xml_object())
            self.element.append(logs)
        return copy.copy(self.element)
